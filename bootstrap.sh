git clone https://github.com/Interview-demoapp/Flasky.git
virtualenv -p python venv
echo export PYTHONPATH="$PYTHONPATH:`pwd`/src/PageObjects:`pwd`/src/RestApi" >> venv/bin/activate
source venv/bin/activate
pip install -r requirements.txt
webdrivermanager firefox chrome --linkpath venv/bin
deactivate
