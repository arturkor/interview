*** Settings ***
Documentation   A test suite for invalid auths

Library         RestApi.RestApiClient
Resource        restapi.resource


Test Template   Auth With Invalid Credentials Should Fail


*** Test Cases ***      USERNAME        PASSWORD
Empty Name Ok Pass      ${EMPTY}        ${VALID_PASS}
Bad Name Ok Pass        bla             ${VALID_PASS}
Ok Name Empty Pass      ${VALID_NAME}   ${EMPTY}
Ok Name Bad Pass        ${VALID_NAME}   bla
Empty Empty             ${EMPTY}        ${EMPTY}


