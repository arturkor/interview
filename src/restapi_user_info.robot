*** Settings ***
Documentation   A test suite for quering user info


Library         RestApi.RestApiClient
Resource        restapi.resource

Test Setup      Setup Restapi Client        ${URL}  ${VALID_NAME}       ${VALID_PASS}


*** Test Cases ***

Query User Info Valid Token:
    Auth Success
    Check User Info     arturk  Artur   K       044

Test User Sql Injection
    Auth Success
    Test Userinfo Sqli
