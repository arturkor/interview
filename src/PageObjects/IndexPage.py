from robot.libraries.BuiltIn import BuiltIn
from BasePage import BasePage


locators = {
    "Log In": "xpath: //a[@href=\"/login\"]",
    "Username": "username",
}


def _seleniumlib():
    return BuiltIn().get_library_instance("SeleniumLibrary")


class IndexPage(BasePage):

    def navigate_to_log_in_page(self):
        _seleniumlib().click_element(locators["Log In"])
        _seleniumlib().wait_until_page_contains_element(locators["Username"])
