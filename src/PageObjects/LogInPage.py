from robot.libraries.BuiltIn import BuiltIn
from BasePage import BasePage


locators = {
    "Username": "id:username",
    "Password": "id:password",
    "Log In": "xpath: //input[@value=\"Log In\"]",
    "SignedUsername": "xpath: //td[@id=\"username\"][contains(text(),'%s')]"
}


def _seleniumlib():
    return BuiltIn().get_library_instance("SeleniumLibrary")


class LogInPage(BasePage):
    
    def log_in(self, username, password):
        self.username = username
        _seleniumlib().set_focus_to_element(locators["Username"])
        _seleniumlib().press_keys(locators["Username"], username)
        _seleniumlib().press_keys(locators["Password"], password)
        _seleniumlib().click_element(locators["Log In"])

    def verify_login_success(self):
        _seleniumlib().wait_until_page_contains_element(locators['SignedUsername'] % (self.username), 10)

    def verify_login_fail(self):
        _seleniumlib().wait_until_page_contains("You provided incorrect login details")
