from robot.libraries.BuiltIn import BuiltIn


locators = {
    "Demo App Banner": "xpath: //a[contains(text(), 'Demo app')]",
    "Index header": "xpath: //h1[contains(text(), 'index page')]"
}


def _seleniumlib():
    return BuiltIn().get_library_instance("SeleniumLibrary")


class BasePage(object):

    def navigate_to_index_page(self):
        _seleniumlib().click_element(locators["Demo App Banner"])
        _seleniumlib().wait_until_page_contains_element(locators["Index header"], 10)
    
