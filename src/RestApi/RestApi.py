from robot.libraries.BuiltIn import BuiltIn
from robot.api.deco import keyword
import requests
try:
    import urllib.parse as urllib
except:
    import urllib


SQLI_USER_TEMPLATES = ["api/users/%s' or '1'='1"]


def _reql():
    return BuiltIn().get_library_instance("RequestsLibrary")

def _log(msg):
    BuiltIn().log(msg)

def _check_true(what, msg):
    BuiltIn().should_be_true(what, msg)

def _check_false(what, msg):
    BuiltIn().should_not_be_true(what, msg)

class RestApiClient:
    ROBOT_LIBRARY_SCOPE = 'TEST SUITE'
    
    @keyword
    def setup_restapi_client(self, url, username, password):
        self.url = url
        self.username = username
        self.password = password
        self.token = ""

    def _get(self, uri):
        if (uri == 'api/auth/token'):
            return requests.get("%s/%s" % (self.url, uri), auth=requests.auth.HTTPBasicAuth(self.username, self.password))
        else:
            return requests.get("%s/%s" % (self.url, uri), headers={'Content-Type': 'application/json', 'Token': self.token})
        

    @keyword
    def get_token(self):
        return self.token

    @keyword
    def check_user_info(self, username, firstname, lastname, phone):
        resp = self._get("api/users/%s" % username)
        _check_true(resp.status_code == 200, "Getting user info failed")
        data = resp.json()
        _check_true(data['payload']['firstname'] == firstname, "Firstname is wrong")
        _check_true(data['payload']['lastname'] == lastname, "Lastname is wrong")
        _check_true(data['payload']['phone'] == phone, "Phone is wrong")
            
    @keyword
    def test_userinfo_sqli(self):
        for i in SQLI_USER_TEMPLATES:
            resp = self._get(urllib.quote(i % self.username))
            _log(urllib.quote(i % self.username))
            _log(resp.content)
            if (resp.status_code == 200):
                _log("WARNING: Possible SQL injection")
                data = resp.json()
                _check_false(data['status'] == 'SUCCESS', "SQL Injection")

    @keyword
    def auth_success(self):
        resp = self._get("api/auth/token")
        _check_true(resp.status_code == 200, "Auth request failed")
        data = resp.json()
        _check_true((data["status"] == "SUCCESS"), "Auth failed")
        self.token = data["token"]

    @keyword
    def auth_failed(self):
        resp = requests.get(self.url, auth=requests.auth.HTTPBasicAuth(self.username, self.password))
        BuiltIn().should_be_empty(self.token, "Auth succeded with wrong username and password")


def create_client(uri, username, password):
    return RestApiClient(uri, username, password)


if __name__ == "__main__":
    pass
