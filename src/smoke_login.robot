*** Settings ***

Documentation   Login smoke test

Resource        smoke.resource
Resource        base.resource
Suite Setup     Open Chrome
Suite Teardown  Close All Browsers


*** Test Cases ***

RegisterUser
    Go To               ${HOST}/register
    Wait Until Page Contains Element    xpath: //input[@id="username"]
    Input Text          xpath: //input[@id="username"]        ${VALID_USER}
    Input Text          xpath: //input[@id="password"]        ${VALID_PASS}
    Input Text          xpath: //input[@id="firstname"]       Artur
    Input Text          xpath: //input[@id="lastname"]        K
    Input Text          xpath: //input[@id="phone"]           044
    Click Element       xpath: //input[@type="submit"]
    Page Should Not Contain     Error


Invalid Login
    IndexPage.Navigate To Index Page
    IndexPage.Navigate To Log In Page
    LogInPage.Log In            wrong   wrong
    LogInPage.Verify Login Fail

Valid Login
    IndexPage.Navigate To Index Page
    IndexPage.Navigate To Log In Page
    LoginPage.Log In    ${VALID_USER}       ${VALID_PASS}
    LogInPage.Verify Login Success
