Task:

	Verify mikroblog application in Flask (https://github.com/Interview-demoapp/Flasky.git). Robot Framework has to be used.

Requirements:

	As a UI user I can:
	1: Register through web portal
	2: Review my own user information from the main view
	
	As an API Consumer I can:
	1: Review users registered in system
	2: If authenticated I can get personal information of users
	3: If authenticated I can update personal information of users

Notes:

    There is a rest API availaible and require testing.
    Rest API at least requires token to be specified as part of each request. Could be automated with requests.
    User information is updated using PUT requests
    DB is SQLite
    Python version 2.7
    
    Tests should be ready to be executed upon request from anywhere.
    
    Usability testing is required. Unittest for test methods is required

Building my strategy:

    I have an application that is Flask based. That means it is a microblog in python, MVC based.
    Flask is a minimalistic plarform and by default does not ensure security verifications. It is worth checkin for input sanitizing and cross site scripting. SQL injections are handled by Flask framework so no need to do this kind of testing unless raw SQL queries are used in code. I should shortly do a static testing to see if any issues would be seen from the code.
    Requirements do not ask me to use robot framework but company uses it so UI tests will be done with Robot Framework plus SeleniumLibrary.
    Robot Framework tests use Test Suite automation approach so to keep things consistent I will not use pytest framework (fixtures based). I will stick with default unittest library.
    The product uses Python 2.7 but that does not force me to use Python 2.7 in tests. I will go with Python 3 at least for UI so in future it would be less legacy burden.


Static analysis:

    [Performance] run.sh shows that application is run without WSGI server. Flask is designed as an WSGI application so something like gunicorn should be used to run it through socket and serve it to outside using some server application like nginx.
    [Functional] Also it is designed as a Microservice because there is a docker file. Would be beneficial to run it with docker as well. Maybe it will fail because it is configured to use latest Alpine image which is using Python 3 and app is designed with Python 2.7 in mind.
    [Functional/UI] Nothing strange in CSS styles at first glance.
    [Code structure] Error HTML tempate is used only for login errors. It could be named better, like login_error.html. Application would have other error types which would require different templates.
    [Security] auth form does not use CSRF token. This form is vulnerable to cross site request forgery. Same thing for other forms.
    [UI] auth form does not have limitation for max username and password length. Registration form does not have values validation and limitation so we can enter whatever we want.
    [Security] api.py shows that raw SQL queries are used with user controlable information. User name, phone are not sanitized during input. This code is vulnerable to SQL injections. Code also has full access to DB so any user during registration can create a payload that would leak the DB information or destroy it. In auth.py everything is fine though. Values are inserted with question signs.
    [Functional] Schema.sql shows that TEXT fields are used for all values. Bad design. Text field size is 64 KB, so one row would be almost 0.4 MB. It will quickly eat all the space and will affect performance a lot. Fields should be chars with limited length.


Nihgt 1: Smoke test:

    4 hours passed here. Static analysis, some problems with design. Learning Robot Framework basic functionality. Implementing Page Object design pattern and linking it with Robot Framework scripts

    I need a bootstrap script for environment preparation. It will be bootstrap.sh. In order to keep environment consistent accross different systems virtualenv is needed. Bootstrap script should prepare it. Libraries required for running the app and for testing will be installed into virtualenv using its pip manager. Because I am testing web app with Robot Framework I will need robotframework, robotframework-seleniumlibrary. For non UI based tests I can use requests library. It will be also useful for REST api verification. For doing proper Rest api fuzzing I would use some fuzzer like Radamsa in real project but setting it for this interview project and generating proper templates is too time consuming. In order to provide Robot Framework access to my custom modules I will make bootstrap script to change the venv/bin/activate script so it will generate proper PYTHONPATH variable.
    For a smoke test and in order to study Robot Framework I can write a login test
    Page Object design pattern is to be used because it allows to separate page implementation from the test logic. So with proper page objects tester can concentrate more on good test scenarios.
    


Night 2: Creating tests for API.
    
    4 hours again. I had problems with Flasky in python 3. I created my own python library with keywords for Flasky rest API testing. Also I learned to use templating in Robot Framework to beautify test script
    
    I am using python 3 so I had to fix the encoding in line 57 of api.py for Flasky
    token = str(base64.b64encode(str(random.getrandbits(128)).encode()), "utf-8")
    I could use the RequestsLibrary right from Robot Framework script but in my opinion that would be cheating as you will not be able to learn my level of knowledge from such aproach.
    I created my personal library based on requests library in python.
    There were problems with Flasky itself in python 3 as I mentioned so it took me some time to fix the issue. I created token retrieval test for valid user and couple invalid username and password checks.

Night 3: Creating CI for tests
    
    3 hours. Docker is new thing to me. I strugled to run Chrome browser in docker image so ended up running headless chrome with some additional parameters. Also I implemented CI where it is possible to trigger tests at any time. The bootstrap will pool latest Flasky app and will ran against latest tests. Results are retrievable from pipeline

    it was mentioned that anyone should be able to run the tests upon request. I decided to go deeper with that and created a CI in gitlab based on Docker image of Debian, Headless Chrome configuration. It took me the whole evening. Running chrome in docker images was new experience to me so I did many mistakes.

Day 4: SQL Injection proof. Making pipline collect results even in case of failed tests. Wrapping up done job.

    2 hours so far. I plan to spend 1 more hour to produce a report of done job.
    
    SQL Injection test is added to the restapi users tests. CI is pretty much doing what it should now. Made sure it is possible to run tests in Python 2 and Python 3.
    

**Post Scriptum**

As a family guy I have time only after I put my kids to sleep and do the house work. Still I had pretty much enough time to try Robot Framework in different cases.
I cannot say that writting tests in Robot Framework for experienced developer is easier but this is definitely easier for non developers for as long as libraries are created by developers. It is sometimes hard to translate python logic into Robot Framework scriptable logic so some ugly code hacks are needed. Double debugging is required - for python scripts and for robot scripts.
The general logic definitely can be tested with Robot Framework with built in libraries. But as for specific tests related to traffic interception, injections, Page Object approach testing - those are way easier to implement in just plain Python. But maybe that is because I am not experienced enough with Robot Framework yet.

In python libraries I had to use Robot Framework builtin libraries for selenium driver. I could create my own instance of selenium driver in my library but with builtin functionality I was sure that each robot framework script that will use my code will be able to use its own selenium preconfigured driver with its own desired capabilities.


I started testing with static code analyses. I noticed couple of security bugs there. I proved sql injection with my test.
Then I continued day (read night here) 1 with creating small smoke test. There were two possible approaches. I use only Robot Framework built in libraries and will be able to test all my ideas within 4 days. Or I will try to create my own python code which I will use in robot framework for testing my ideas. With second approach I was not able to do extensive testing in 4 days. But I shoved my coding experience with Python and proved that I am able to push the limits of Robot Frameworks further away.
I tried to implement Page Object tests design pattern in Python and apply it to Robot Framework. It worked even though it looked ugly (I was coding for RF for the first time). But at least I've seen that it is possible.

On day 2 I stated to implement api testing. Again I could do it with RequestsLibrary in RF but no coding skill is shown in this case. I created my own library for Robot Framework based on python requests library. Because library creation for RF took me more time I did not test all API possibilities. So I created tests for positive flow Authentication -> Getting User Information, Verified several cases of Failed Authentication and verified one case of SQL injection on day 4.
It took me again around 4 hours at late night.

On day 3 I wanted to show my understanding of CI/CD approaches. I knew that GitLab has some builtin CI/CD capabilities that are based on docker images. I created CI/CD script that prepares the environment by itself and then bootstraps test environment in order to execute latest test vs latest Flasky that gets clonned by bootstraping.
Day 3 was Saturday but we had a birthday party so again I worked with code at night and not much.

On day 4 I decided to prove my point that rest api is vulnerable to SQL injections. So I tested my idea manually with curl and then added method to my python library that will be capable of running all feeded sql injection templates into Flasky. Also I polished the CI script and was writting the wrap up for the report

I was writting my python code with such idea that user who would be creating test script does not have to worry about page implementation. He only had to think of actions he had to do (Page Object design pattern). So only main input variables are configured through Robot Framework resource file.

Tests that I thought of but did not do:

    SQL injections are possible in all rest api methods with controlable input parameters so all those will fail
    Testing long user names, passwords, etc. But that would be useless from my static analyses results. There could be 64 kb of data in each field and that is a huge bug in implementaion itself. But test for proper limitations can be written before the code is fixed, TDD approach.
    Testing phone number by template.
    Testing SQL injections in forms (those will pass, because implementation of controls for web forms is done propery)
    Testing the posibility of not authenticated user to check information for other user and edit it.
    Editing user information. Here are XSS injections are possible actually. The input data is not sanitized and javascript injections are possible as part of username for example that can lead to hacking other users.
    Register form also does not sanitise input so XSS injections are possible during user creation.
    None of the forms has the CSRF token which makes it possible cross site request forgery attack for the user.
    Rest api access is based on token which can be stollen with XSS for example. It is not JS protected. I would recommend using HttpOnly cookie for keeping token there. In this case XSS will not bring problems at least with token access.
    Performance testing can be done. I don't know how perf testing is done with Robot Framework but JMeter can perfectly do it. It would require different code though. Also can be done with Python and Async libs + several different python processes.

So basically these are my ideas. I was told that home task requires around 2-4 hours to complete but I really don't see how a person could do everything I've written here in 4 hours even if he knows RF good enough.
Finally, because I created a fully featured CI for my homework there is no need to send the files to anyone. Instead register at gitlab and send me username of necessary gitlab users and I will add them to the project. And they will be able to execute CI and see output at any time without bothering with their machines.